using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Grabbable))]
public class BarController : MonoBehaviour
{
    private enum Phase { goUp, goDown };
    [SerializeField] private float yDistanceHeadChest = 0.1f;
    [SerializeField] [TextArea] private string grabInstructions;
    [SerializeField] [TextArea] private string toTheTopInstructions;
    [SerializeField] [TextArea] private string letsGoInstructions;
    [SerializeField] [TextArea] private string upInstructions;
    [SerializeField] [TextArea] private string downInstructions;
    [SerializeField] [TextArea] private string keepBarFlatInstructions;
    [SerializeField] [TextArea] private string strongerInstructions;
    [SerializeField] [TextArea] private string newHighscoreInstructions;
    [SerializeField] private float letsGoDisplayTime = 1f;
    [SerializeField] private float strongerTime = 1f;
    [SerializeField] private float strongerDelay = 0.5f;
    [SerializeField] private float countDisplayTime = 2f;
    [SerializeField] private float keepBarFlatDelay = 0.5f;
    [SerializeField] [Range(0f, 1f)] private float slopeTreshold = 0.5f;
    [SerializeField] private float struggleTreshold = 1f;
    [SerializeField] private float slopeVibrationFreq = 0.8f;
    [SerializeField] private float struggleVibrationFreq = 0.2f;
    [SerializeField] private float normalEnergyConsumption = 0.05f;
    [SerializeField] private float slopeEnergyConsumption = 0.1f;
    [SerializeField] private float normalPerFrameEnergyConsumption = 0.0005f;
    [SerializeField] private float maxYTolerance = 0.1f;
    [SerializeField] private int celebrationRate = 5;
    [SerializeField] private float scoreTime = 5f;
    [SerializeField] private ParticleSystem celebrationParticle;
    [SerializeField] private AudioClip celebrationSound;
    [SerializeField] private AudioClip highscoreSound;
    private Grabbable grabbable;
    private List<GameObject> fakeWeights = new List<GameObject>();
    private float maxY;
    private float angleTolerance;
    private bool isGrabbed = false;
    private bool exerciseStarted = false;
    private Phase currentPhase;
    private int ExerciseCount
    {
        get
        {
            return _exerciseCount;
        }

        set
        {
            if(_exerciseCount != value)
            {
                if(value > 0)
                    SetText(value.ToString(), countDisplayTime, 2);
                if(value % celebrationRate == 0 && value > 0)
                {
                    celebrationParticle.Play();
                    GameManager.instance.sfxEmitter.PlayOneShot(celebrationSound);
                }
            }
            _exerciseCount = value;
        }
    }
    private int _exerciseCount = 0;

    private class TextContent
    {
        public string text;
        public float startTime;
        public float duration;
        public float confirmation;
        public int importance;

        public TextContent(string text, float startTime, float duration, float confirmation, int importance)
        {
            this.text = text;
            this.startTime = startTime;
            this.duration = duration;
            this.confirmation = confirmation;
            this.importance = importance;
        }
    }


    private List<TextContent> textConfirmationQueue = new List<TextContent>();
    private List<TextContent> textQueue = new List<TextContent>();
    private TextContent currentText = null;

    private void Start()
    {
        grabbable = GetComponent<Grabbable>();
        grabbable.onGrab.AddListener(OnGrab);
        grabbable.onDrop.AddListener(OnDrop);
        GameManager.instance.onEnergy0.AddListener(OnEnergy0);
    }

    private void Update()
    {
        float heartbeatSpeed = 1f;
        float leftVibration = 0f;
        float rightVibration = 0f;
        float vibrationFreq = 0f;
        if (isGrabbed)
        {
            if (!exerciseStarted)
            {
                // Setting max y
                if (transform.position.y > maxY)
                    maxY = transform.position.y;

                // Start execise
                if (transform.position.y < GameManager.instance.playerHeadset.position.y + yDistanceHeadChest && maxY - maxYTolerance > GameManager.instance.playerHeadset.position.y + yDistanceHeadChest)
                {
                    SetText(letsGoInstructions, letsGoDisplayTime, 2);
                    exerciseStarted = true;
                    GameManager.instance.minSpeed = GameManager.instance.minSpeedK * GameManager.instance.CurrentWeight;
                    angleTolerance = Mathf.Lerp(GameManager.instance.maxAngleTolerance, GameManager.instance.minAngleTolerance,
                                                Mathf.InverseLerp(GameManager.instance.baseWeight, GameManager.instance.maxWeight, GameManager.instance.CurrentWeight));
                    currentPhase = Phase.goUp;
                    ExerciseCount = 0;
                }
            }
            else
            {

                float currentStruggle = 0f;
                float currentMovement = 0f;
                bool handsArrived = true;
                foreach (HandController hand in GameManager.instance.playerHands)
                {
                    currentStruggle += hand.struggle / Time.deltaTime;
                    currentMovement += hand.movement;
                    if (hand.anchor.position.y < maxY - maxYTolerance)
                        handsArrived = false;
                }

                float currentEnergyConsumption = normalEnergyConsumption * currentMovement;
                if (currentPhase == Phase.goUp)
                {
                    SetText(upInstructions);
                    if (transform.position.y > maxY - maxYTolerance)
                    {
                        ExerciseCount++;
                        currentPhase = Phase.goDown;
                    }
                    else
                    {
                        if(handsArrived)
                        {
                            SetText(strongerInstructions, strongerTime, 2, strongerDelay);
                        }
                    }
                }

                else if (currentPhase == Phase.goDown)
                {
                    SetText(downInstructions);
                    if (transform.position.y < GameManager.instance.playerHeadset.position.y + yDistanceHeadChest)
                    {
                        currentPhase = Phase.goUp;
                    }
                }

                float currentSlope = Vector3.SignedAngle(Vector3.right, transform.right, Vector3.forward) / angleTolerance;

                if (Mathf.Abs(currentSlope) > slopeTreshold)
                {
                    if(Mathf.Abs(currentSlope) > 1f)
                    {
                        SetText(keepBarFlatInstructions, 1f, 3, 0f);
                        grabbable.ForceDrop();
                    }
                    else
                    {
                        SetText(keepBarFlatInstructions, 0f, 2, keepBarFlatDelay);
                    }
                    currentEnergyConsumption += slopeEnergyConsumption * Time.deltaTime;

                    if (currentSlope > 0)
                        leftVibration = Mathf.Abs(currentSlope);
                    else
                        rightVibration = Mathf.Abs(currentSlope);
                    vibrationFreq = slopeVibrationFreq;
                }
                else if (currentStruggle > struggleTreshold)
                {
                    leftVibration = 1f;
                    rightVibration = 1f;
                    vibrationFreq = struggleVibrationFreq;
                }
                heartbeatSpeed = 1f + (currentEnergyConsumption / normalPerFrameEnergyConsumption);
                GameManager.instance.Energy -= currentEnergyConsumption;
            }
        }
        else
        {
            SetText(grabInstructions, 0f, 1, scoreTime);
            if (transform.position.y < GameManager.instance.playerHeadset.position.y)
            {
                grabbable.ResetGrabbable();
                ShowScore();
                GameManager.instance.Energy = 1f;
            }
        }

        OVRInput.SetControllerVibration(vibrationFreq, leftVibration, OVRInput.Controller.LTouch);
        OVRInput.SetControllerVibration(vibrationFreq, rightVibration, OVRInput.Controller.RTouch);
        GameManager.instance.heartAnimator.SetFloat(GameManager.instance.heartAnimatorSpeedParameter, heartbeatSpeed);
    }

    private void LateUpdate()
    {
        TextContent prevText = currentText;
        List<TextContent> toRemove = new List<TextContent>();
        foreach (TextContent t in textConfirmationQueue)
        {
            if(!textQueue.Exists(x => x.text==t.text))
            {
                toRemove.Add(t);
            }
        }

        foreach(TextContent t in toRemove)
        {
            textConfirmationQueue.Remove(t);
        }
        toRemove.Clear();


        List<TextContent> possibleTexts = new List<TextContent>();

        foreach (TextContent t in textQueue)
        {
            if(Time.time >= t.startTime)
            {
                if(t.confirmation == 0f)
                {
                    possibleTexts.Add(t);
                }
                else
                {
                    TextContent textConfirmation = textConfirmationQueue.Find(x => x.text == t.text);
                    if(textConfirmation == null)
                    {
                        textConfirmationQueue.Add(t);
                    }
                    else
                    {
                        if(Time.time > textConfirmation.startTime + textConfirmation.confirmation)
                        {
                            possibleTexts.Add(t);
                        }
                    }
                }

                toRemove.Add(t);
            }
        }

        foreach (TextContent t in toRemove)
        {
            textQueue.Remove(t);
        }
        toRemove.Clear();

        if (currentText == null || !textQueue.Exists(x => x.text == currentText.text))
        {
            int currentImportance = -1;

            if (currentText != null && Time.time < currentText.startTime + currentText.confirmation + currentText.duration)
            {
                currentImportance = currentText.importance;
            }
            foreach (TextContent t in possibleTexts)
            {
                if (t.importance < currentImportance)
                    continue;

                currentText = t;
                currentImportance = t.importance;
            }
        }


        if (currentText != null)
            GameManager.instance.exerciseStartText.text = currentText.text;
    }

    private void OnEnable()
    {
        foreach(GameObject fakeWeight in fakeWeights)
        {
            Destroy(fakeWeight);
        }
        fakeWeights.Clear();

        foreach(WeightController weight in GameManager.instance.weightsOnBar)
        {
            GameObject fakeWeight = Instantiate(weight.gameObject, transform);
            fakeWeight.transform.position = weight.transform.position;
            fakeWeight.transform.rotation = weight.transform.rotation;
            fakeWeight.transform.localScale = weight.transform.localScale;
            Destroy(fakeWeight.GetComponent<WeightController>());
            Destroy(fakeWeight.GetComponent<Grabbable>());
            Destroy(fakeWeight.GetComponent<Rigidbody>());
            Destroy(fakeWeight.GetComponent<Collider>());
            GameObject fakeWeightOpposite = Instantiate(fakeWeight, transform);
            fakeWeightOpposite.transform.localPosition = new Vector3(-fakeWeightOpposite.transform.localPosition.x,
                                                                     fakeWeightOpposite.transform.localPosition.y,
                                                                     fakeWeightOpposite.transform.localPosition.z);
            fakeWeightOpposite.transform.rotation = weight.transform.rotation;
            fakeWeightOpposite.transform.localScale = weight.transform.localScale;
            Destroy(fakeWeightOpposite.GetComponent<WeightController>());
            Destroy(fakeWeightOpposite.GetComponent<Grabbable>());
            Destroy(fakeWeightOpposite.GetComponent<Rigidbody>());
            Destroy(fakeWeightOpposite.GetComponent<Collider>());

            fakeWeights.Add(fakeWeight);
            fakeWeights.Add(fakeWeightOpposite);
        }
        
        GameManager.instance.exerciseStartText.gameObject.SetActive(true);
        SetText(grabInstructions, 0f, 2);
    }

    private void OnGrab()
    {
        isGrabbed = true;
        exerciseStarted = false;
        maxY = transform.position.y;
        SetText(toTheTopInstructions, 0f, 2);
    }

    private void OnDrop()
    {
        isGrabbed = false;
        exerciseStarted = false;
        GameManager.instance.minSpeed = 0f;
    }

    private void OnEnergy0()
    {
        grabbable.ForceDrop();
        grabbable.ResetGrabbable();
        ShowScore();
    }

    private void SetText(string text, float time = 0f, int importance = 0, float confirmationTime = 0f, bool asap = false)
    {
        float startTime = Time.time;
        if (asap && currentText.importance >= importance)
        {
            startTime = currentText.startTime + currentText.confirmation + currentText.duration + Time.deltaTime;
        }
        textQueue.Add(new TextContent(text, startTime, time, confirmationTime, importance));
    }

    private void ShowScore()
    {
        bool newHighScore = false;
        GameManager.instance.AddScore(ExerciseCount, ref newHighScore);
        string score = ExerciseCount.ToString();
        if (newHighScore)
        {
            score += newHighscoreInstructions;
            GameManager.instance.sfxEmitter.PlayOneShot(highscoreSound);
        }
        SetText(score, 0f, 2, 0f, true);
    }
}
