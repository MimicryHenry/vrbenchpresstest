using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public OVRCameraRig playerCameraRig;
    public Transform playerHeadset;
    public HandController[] playerHands;
    public OVRScreenFade fade;
    public float fadeTime = 0.5f;
    public LayerMask grabbabblesLayer;
    public LayerMask weightSnappingPointLayer;
    public float weightSnapRange = 0.5f;
    public float weightSnapSpeed = 1f;
    public float weightSnapRotationSpeed = 90f;
    public float baseWeight = 10f;
    public float maxWeight = 45f;
    public Gradient difficultyColorGradient;
    public Text currentWeightText;
    public float layDownMaxY = 0.5f;
    public float layDownAngleTolerance = 10f;
    public float changePositionTime = 3f;
    public Transform layDownPosition;
    public GameObject[] layDownObjects;
    public GameObject[] standUpObjects;
    public bool IsLayingDown
    {
        get
        {
            if(playerHeadset.position.y - playerCameraRig.transform.position.y < layDownMaxY && 
               Vector3.Angle(playerHeadset.forward, Vector3.up) < layDownAngleTolerance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public float CurrentWeight
    {
        get
        {
            return _currentWeight;
        }

        set
        {
            if(currentWeightText)
            {
                currentWeightText.text = value.ToString("F0");
                currentWeightText.color = difficultyColorGradient.Evaluate((value - baseWeight) / (maxWeight - baseWeight));
            }
            _currentWeight = value;
            if (currentHighscoreText)
            {
                currentHighscoreText.text = GetHighscore().ToString();
            }
        }
    }
    private float _currentWeight;

    public float maxAngleTolerance = 20f;
    public float minAngleTolerance = 10f;
    public float minSpeedK = 0.05f;
    public AnimationCurve underMinSpeedEffect;
    public Text exerciseStartText;
    public Text currentHighscoreText;
    public Text debugText;
    public UnityEvent onEnergy0;
    public Image energyBar;
    public Animator heartAnimator;
    public string heartAnimatorSpeedParameter = "speed";
    public AudioSource sfxEmitter;

    [HideInInspector] public float minSpeed = 0f;

    [HideInInspector] public ObservableCollection<WeightController> weightsOnBar = new ObservableCollection<WeightController>();
    [HideInInspector] public float Energy
    {
        get
        {
            return energy;
        }

        set
        {
            energyBar.fillAmount = value;
            energy = value;
            if(energy<=0f)
            {
                onEnergy0.Invoke();
                energy = 1f;
            }
        }
    }
    private float energy = 1f;

    private float changePositionStartTime;
    private bool currentlyLayingDown = false;
    private Transform standUpPosition;
    private Coroutine changePositionCoroutine = null;
    private Dictionary<float, int> highscores = new Dictionary<float, int>();

    void Awake()
    {
        instance = this;
        standUpPosition = new GameObject("StandUpPosition").transform;
        standUpPosition.position = playerCameraRig.transform.position;
        standUpPosition.rotation = playerCameraRig.transform.rotation;

        weightsOnBar.CollectionChanged += OnWeightsListChanged;
    }

    private void Start()
    {
        CurrentWeight = baseWeight;

        foreach (GameObject gO in standUpObjects)
        {
            gO.SetActive(true);
        }
        foreach (GameObject gO in layDownObjects)
        {
            gO.SetActive(false);
        }
    }

    private void Update()
    {
        if (currentlyLayingDown == IsLayingDown)
        {
            changePositionStartTime = Time.time;
        }
        else
        {
            if (Time.time - changePositionStartTime > changePositionTime && changePositionCoroutine==null)
            {
                if (currentlyLayingDown)
                {
                    changePositionCoroutine = StartCoroutine(OnStandUp());
                }
                else
                {
                    changePositionCoroutine = StartCoroutine(OnLayDown());
                }

            }
        }
    }

    private IEnumerator OnLayDown()
    {
        fade.fadeTime = fadeTime;
        fade.FadeOut();

        yield return new WaitForSeconds(fadeTime);

        currentlyLayingDown = true;
        playerCameraRig.transform.Rotate(Vector3.up, Vector3.SignedAngle(Vector3.ProjectOnPlane(playerHeadset.up, Vector3.up), Vector3.ProjectOnPlane(layDownPosition.up, Vector3.up), Vector3.up));
        playerCameraRig.transform.position = layDownPosition.position - (playerHeadset.position - playerCameraRig.transform.position);
        changePositionStartTime = Time.time;
        foreach(GameObject gO in layDownObjects)
        {
            gO.SetActive(true);
        }
        foreach (GameObject gO in standUpObjects)
        {
            gO.SetActive(false);
        }
        fade.FadeIn();

        yield return new WaitForSeconds(fadeTime);

        changePositionCoroutine = null;
    }

    private IEnumerator OnStandUp()
    {
        fade.fadeTime = fadeTime;
        fade.FadeOut();

        yield return new WaitForSeconds(fadeTime);

        currentlyLayingDown = false;
        playerCameraRig.transform.position = standUpPosition.position;
        playerCameraRig.transform.rotation = standUpPosition.rotation;
        changePositionStartTime = Time.time;
        minSpeed = 0f;
        foreach (GameObject gO in standUpObjects)
        {
            gO.SetActive(true);
        }
        foreach (GameObject gO in layDownObjects)
        {
            gO.SetActive(false);
        }
        fade.FadeIn();

        yield return new WaitForSeconds(fadeTime);

        changePositionCoroutine = null;
    }

    private void OnWeightsListChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add ||
            e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
        {
            foreach(WeightController weight in e.NewItems)
            {
                CurrentWeight += weight.weight;
            }
        }

        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove ||
            e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
        {
            foreach (WeightController weight in e.OldItems)
            {
                CurrentWeight -= weight.weight;
            }
        }
    }

    public void AddScore(int score, ref bool isNewHighscore)
    {
        if (!highscores.ContainsKey(CurrentWeight))
        {
            highscores.Add(CurrentWeight, score);
            isNewHighscore = false;
            if (currentHighscoreText)
            {
                currentHighscoreText.text = GetHighscore().ToString();
            }
        }
        else
        {
            if (highscores[CurrentWeight] < score)
            {
                highscores[CurrentWeight] = score;
                isNewHighscore = true;
                if (currentHighscoreText)
                {
                    currentHighscoreText.text = GetHighscore().ToString();
                }
            }
            else
                isNewHighscore = false;
        }
    }

    public int GetHighscore()
    {
        if (!highscores.ContainsKey(CurrentWeight))
            return 0;
        else
            return highscores[CurrentWeight];
    }
}
