using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour
{
    [SerializeField] private OVRInput.Controller m_controller;
    [SerializeField] [Range(0f, 1f)] private float grabSensitivity = 0.5f;
    [SerializeField] private float grabRange = 0.25f;

    private Grabbable currentGrabbable = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float currentInput = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

        // Stop Grabbing
        if(currentGrabbable && ((Application.isEditor? Input.GetKeyDown(KeyCode.V) : currentInput<grabSensitivity)))
        {
            currentGrabbable.EndGrab(transform);
            currentGrabbable = null;
        }

        // Start Grabbing
        if(!currentGrabbable && ((Application.isEditor ? Input.GetKeyDown(KeyCode.C) : currentInput > grabSensitivity)))
        {
            Debug.Log("Grab");
            Grabbable grabbedGrabbable = null;
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, grabRange, GameManager.instance.grabbabblesLayer, QueryTriggerInteraction.Collide);
            float currentMinDistance = Mathf.Infinity;
            foreach(Collider hitCollider in hitColliders)
            {
                Grabbable hitGrabbable = hitCollider.GetComponent<Grabbable>();
                if(hitGrabbable)
                {
                    float distance = Vector3.Distance(transform.position, hitCollider.ClosestPoint(transform.position));
                    if(distance < currentMinDistance)
                    {
                        grabbedGrabbable = hitGrabbable;
                        currentMinDistance = distance;
                    }
                }
            }

            if(grabbedGrabbable)
            {
                grabbedGrabbable.Grab(transform);
                currentGrabbable = grabbedGrabbable;
            }
        }
    }
}
