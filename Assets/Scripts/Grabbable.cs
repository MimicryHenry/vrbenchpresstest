using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour
{
    [SerializeField] public UnityEvent onGrab;
    [SerializeField] public UnityEvent onDrop;
    [SerializeField] private bool needsBothHands = false;
    [SerializeField] private bool activateGravityOnDrop = false;

    private class GrabPoint
    {
        public Transform grabber = null;
        public GameObject point;

        public GrabPoint(Transform _grabber, Grabbable _grabbable)
        {
            grabber = _grabber;
            point = new GameObject("grabPoint");
            point.transform.parent = _grabbable.transform;
            point.transform.position = _grabber.position;
            point.transform.rotation = _grabber.rotation;
        }
    }

    private List<GrabPoint> grabPoints = new List<GrabPoint>();

    private Rigidbody myRb;
    private Collider myCol;
    private WeightSnapPoint SnapPoint
    {
        get
        {
            return _snapPoint;
        }
        set
        {
            if(value!= _snapPoint)
            {
                if(_snapPoint)
                    _snapPoint.CurrentGrabbable = null;
                if(value)
                    value.CurrentGrabbable = this;
                if(value == null)
                {
                    myRb.velocity = Vector3.zero;
                    myRb.angularVelocity = Vector3.zero;
                    myRb.useGravity = startUseGravity;
                    myCol.isTrigger = false;
                }
                else
                {
                    myRb.velocity = Vector3.zero;
                    myRb.angularVelocity = Vector3.zero;
                    myRb.useGravity = false;
                    myCol.isTrigger = true;
                }
            }
            _snapPoint = value;
        }
    }
    private WeightSnapPoint _snapPoint;
    private bool isBeingGrabbed = false;
    private bool startUseGravity;
    private Vector3 startPosition;
    private Quaternion startRotation;
    private int minGrabPoints = 1;

    private void Awake()
    {
        myRb = GetComponent<Rigidbody>();
        myCol = GetComponent<Collider>();
    }

    private void Start()
    {
        if(needsBothHands)
        {
            minGrabPoints = 2;
        }

        startUseGravity = myRb.useGravity;
        startPosition = transform.position;
        startRotation = transform.rotation;

        TrySnapping();
        if (SnapPoint)
        {
            transform.position = SnapPoint.transform.position;
            transform.rotation = SnapPoint.transform.rotation;
        }
    }

    private void Update()
    {
        // Grabbing
        if(grabPoints.Count==1 && !needsBothHands)
        {
            transform.rotation = grabPoints[0].grabber.rotation * Quaternion.Inverse(grabPoints[0].point.transform.localRotation);
            transform.position = grabPoints[0].grabber.position - (grabPoints[0].point.transform.transform.position - transform.position);
        }
        else if(grabPoints.Count==2)
        {
            transform.rotation = grabPoints[0].grabber.rotation * Quaternion.Inverse(grabPoints[0].point.transform.localRotation);
            transform.position = grabPoints[0].grabber.position - (grabPoints[0].point.transform.transform.position - transform.position);
            Vector3 grabPointsLine = grabPoints[0].grabber.position - grabPoints[1].grabber.position;
            Vector3 grabbersLine = grabPoints[0].point.transform.transform.position - grabPoints[1].point.transform.transform.position;
            Vector3 perpendicularLine = Vector3.Cross(grabPointsLine, grabbersLine).normalized;
            transform.RotateAround(grabPoints[0].grabber.position, perpendicularLine, Vector3.SignedAngle(grabbersLine, grabPointsLine, perpendicularLine));
        }
        else if(grabPoints.Count>2)
        {
            Debug.LogWarning("More then 2 hands?!");
        }

        // Snapping
        if(SnapPoint && (transform.position!=SnapPoint.transform.position || transform.rotation!=SnapPoint.transform.rotation))
        {
            transform.position = Vector3.MoveTowards(transform.position, SnapPoint.transform.position, GameManager.instance.weightSnapSpeed*Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, SnapPoint.transform.rotation, GameManager.instance.weightSnapRotationSpeed*Time.deltaTime);
        }
    }

    public void Grab(Transform grabber)
    {
        grabPoints.Add(new GrabPoint(grabber, this));

        if (grabPoints.Count >= minGrabPoints && !isBeingGrabbed)
        {
            isBeingGrabbed = true;
            SnapPoint = null;
            myRb.velocity = Vector3.zero;
            myRb.angularVelocity = Vector3.zero;
            myRb.useGravity = false;
            onGrab.Invoke();
        }

    }


    public void EndGrab(Transform grabber)
    {
        grabPoints.RemoveAll(x => x.grabber == grabber);
        if (grabPoints.Count < minGrabPoints && isBeingGrabbed)
        {
            isBeingGrabbed = false;
            myRb.velocity = Vector3.zero;
            myRb.angularVelocity = Vector3.zero;
            if (!activateGravityOnDrop)
                myRb.useGravity = startUseGravity;
            else
                myRb.useGravity = true;
            TrySnapping();
            onDrop.Invoke();
        }
    }

    private void TrySnapping()
    {

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, GameManager.instance.weightSnapRange, GameManager.instance.weightSnappingPointLayer, QueryTriggerInteraction.Collide);
        float currentMinDistance = Mathf.Infinity;
        WeightSnapPoint bestSnapPoint = null;
        foreach (Collider hitCollider in hitColliders)
        {
            float distance = Vector3.Distance(transform.position, hitCollider.transform.position);
            if (distance < currentMinDistance)
            {

                WeightSnapPoint hitSnapPoint = hitCollider.GetComponent<WeightSnapPoint>();
                if (hitSnapPoint && hitSnapPoint.CurrentGrabbable == null)
                {
                    bestSnapPoint = hitSnapPoint;
                    currentMinDistance = distance;
                }
            }
        }

        if (bestSnapPoint == null)
            return;

        SnapPoint = bestSnapPoint;
    }

    public void ForceDrop()
    {
        grabPoints.Clear();
        EndGrab(null);
    }

    public void ResetGrabbable()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
        myRb.useGravity = startUseGravity;
        myRb.velocity = Vector3.zero;
        myRb.angularVelocity = Vector3.zero;

        TrySnapping();
        if (SnapPoint)
        {
            transform.position = SnapPoint.transform.position;
            transform.rotation = SnapPoint.transform.rotation;
        }
    }


}
