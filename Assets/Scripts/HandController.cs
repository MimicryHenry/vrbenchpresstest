using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandController : MonoBehaviour
{
    public const string ANIM_LAYER_NAME_POINT = "Point Layer";
    public const string ANIM_LAYER_NAME_THUMB = "Thumb Layer";
    public const string ANIM_PARAM_NAME_FLEX = "Flex";
    public const string ANIM_PARAM_NAME_POSE = "Pose";
    public const float THRESH_COLLISION_FLEX = 0.9f;

    public const float INPUT_RATE_CHANGE = 20.0f;

    public const float COLLIDER_SCALE_MIN = 0.01f;
    public const float COLLIDER_SCALE_MAX = 1.0f;
    public const float COLLIDER_SCALE_PER_SECOND = 1.0f;

    public const float TRIGGER_DEBOUNCE_TIME = 0.05f;
    public const float THUMB_DEBOUNCE_TIME = 0.15f;

    [SerializeField] private OVRInput.Controller m_controller = OVRInput.Controller.None;
    [SerializeField] private Animator m_animator = null;
    [SerializeField] public Transform anchor = null;

    List<Renderer> m_showAfterInputFocusAcquired;

    private int m_animLayerIndexThumb = -1;
    private int m_animLayerIndexPoint = -1;
    private int m_animParamIndexFlex = -1;
    private int m_animParamIndexPose = -1;

    private bool m_isPointing = false;
    private bool m_isGivingThumbsUp = false;
    private float m_pointBlend = 0.0f;
    private float m_thumbsUpBlend = 0.0f;
    private Vector3 oldAnchorPos;

    private bool m_restoreOnInputAcquired = false;

    private Quaternion startRotation;
    private HandController otherHand;

    [HideInInspector] public float movement;
    [HideInInspector] public float struggle;

    private void Awake()
    {
    }

    private void Start()
    {
        startRotation = transform.rotation;
        oldAnchorPos = anchor.position;

        m_showAfterInputFocusAcquired = new List<Renderer>();

        // Get animator layer indices by name, for later use switching between hand visuals
        m_animLayerIndexPoint = m_animator.GetLayerIndex(ANIM_LAYER_NAME_POINT);
        m_animLayerIndexThumb = m_animator.GetLayerIndex(ANIM_LAYER_NAME_THUMB);
        m_animParamIndexFlex = Animator.StringToHash(ANIM_PARAM_NAME_FLEX);
        m_animParamIndexPose = Animator.StringToHash(ANIM_PARAM_NAME_POSE);

        OVRManager.InputFocusAcquired += OnInputFocusAcquired;
        OVRManager.InputFocusLost += OnInputFocusLost;

        if (GameManager.instance.playerHands.Length == 2)
        {
            if (otherHand == GameManager.instance.playerHands[0])
                otherHand = GameManager.instance.playerHands[1];
            else
                otherHand = GameManager.instance.playerHands[0];
        }
        else
        {
            Debug.LogWarning("Hands in number different then 2?!");
        }

    }

    private void OnDestroy()
    {
        OVRManager.InputFocusAcquired -= OnInputFocusAcquired;
        OVRManager.InputFocusLost -= OnInputFocusLost;
    }

    private void Update()
    {
        movement = Vector3.Distance(anchor.position, oldAnchorPos);

        //Update Transform
        transform.rotation = anchor.rotation * startRotation;
        float newY = anchor.position.y;
        float dPosY = anchor.position.y - oldAnchorPos.y;
        if(anchor.position.y > transform.position.y && anchor.position.y > otherHand.transform.position.y && GameManager.instance.minSpeed > 0f)
        {
            if(dPosY < GameManager.instance.minSpeed * Time.deltaTime)
            {
                dPosY *= GameManager.instance.underMinSpeedEffect.Evaluate(Mathf.InverseLerp(0f, GameManager.instance.minSpeed * Time.deltaTime, dPosY));
            }

            newY = transform.position.y + dPosY;
        }

        Vector3 newPosition = new Vector3(anchor.position.x, newY, anchor.position.z);

        struggle = movement - Vector3.Distance(newPosition, transform.position);

        transform.position = newPosition;
       
                                         
        oldAnchorPos = anchor.position;

        //Update Animation
        UpdateCapTouchStates();

        m_pointBlend = InputValueRateChange(m_isPointing, m_pointBlend);
        m_thumbsUpBlend = InputValueRateChange(m_isGivingThumbsUp, m_thumbsUpBlend);

        float flex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);

        UpdateAnimStates();
    }

    // Just checking the state of the index and thumb cap touch sensors, but with a little bit of
    // debouncing.
    private void UpdateCapTouchStates()
    {
        m_isPointing = !OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, m_controller);
        m_isGivingThumbsUp = !OVRInput.Get(OVRInput.NearTouch.PrimaryThumbButtons, m_controller);
    }


    // Simple Dash support. Just hide the hands.
    private void OnInputFocusLost()
    {
        if (gameObject.activeInHierarchy)
        {
            m_showAfterInputFocusAcquired.Clear();
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; ++i)
            {
                if (renderers[i].enabled)
                {
                    renderers[i].enabled = false;
                    m_showAfterInputFocusAcquired.Add(renderers[i]);
                }
            }

            m_restoreOnInputAcquired = true;
        }
    }

    private void OnInputFocusAcquired()
    {
        if (m_restoreOnInputAcquired)
        {
            for (int i = 0; i < m_showAfterInputFocusAcquired.Count; ++i)
            {
                if (m_showAfterInputFocusAcquired[i])
                {
                    m_showAfterInputFocusAcquired[i].enabled = true;
                }
            }
            m_showAfterInputFocusAcquired.Clear();

            // Update function will update this flag appropriately. Do not set it to a potentially incorrect value here.
            //CollisionEnable(true);

            m_restoreOnInputAcquired = false;
        }
    }

    private float InputValueRateChange(bool isDown, float value)
    {
        float rateDelta = Time.deltaTime * INPUT_RATE_CHANGE;
        float sign = isDown ? 1.0f : -1.0f;
        return Mathf.Clamp01(value + rateDelta * sign);
    }

    private void UpdateAnimStates()
    {
        // Flex
        // blend between open hand and fully closed fist
        float flex = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, m_controller);
        m_animator.SetFloat(m_animParamIndexFlex, flex);

        // Point
        float point = m_pointBlend;
        m_animator.SetLayerWeight(m_animLayerIndexPoint, point);

        // Thumbs up
        float thumbsUp = m_thumbsUpBlend;
        m_animator.SetLayerWeight(m_animLayerIndexThumb, thumbsUp);

        float pinch = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, m_controller);
        m_animator.SetFloat("Pinch", pinch);
    }
    
}
