using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WeightSnapPoint : MonoBehaviour
{
    [SerializeField] private WeightSnapPoint nextPoint;
    [SerializeField] private bool onBar = false;
    [HideInInspector] public WeightSnapPoint prevPoint;
    [HideInInspector] public Grabbable CurrentGrabbable
    {
        get
        {
            return currentGrabbable;
        }
        set
        {
            if(prevPoint && prevPoint.CurrentGrabbable)
            {
                prevPoint.CurrentGrabbable.GetComponent<Collider>().enabled = (value == null) ? true : false;
            }
            if(nextPoint)
            {
                nextPoint.gameObject.SetActive((value != null) ? true : false);
            }

            if (onBar)
            {
                if (value != null)
                {
                    WeightController weight = value.GetComponent<WeightController>();
                    if (weight)
                    {
                        GameManager.instance.weightsOnBar.Add(weight);
                    }
                }

                if (currentGrabbable != null)
                {
                    WeightController weight = currentGrabbable.GetComponent<WeightController>();
                    if (weight)
                    {
                        GameManager.instance.weightsOnBar.Remove(weight);
                    }
                }
            }

            currentGrabbable = value;
        }
    }
    private Grabbable currentGrabbable = null;

    private void Start()
    {
        if (nextPoint)
        {
            nextPoint.prevPoint = this;
            nextPoint.gameObject.SetActive(false);
        }
    }
}
